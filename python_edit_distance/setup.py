#!/usr/bin/python3

import distutils.core

mod = distutils.core.Extension("editdistance", sources=["editdistance.c"])
distutils.core.setup(
    name = "editdistance",
    version = "1",
    description = "levenshtein distance",
    ext_modules = [mod]
)
