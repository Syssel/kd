#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <wchar.h>

#include <Python.h>

struct sub_weight {
	long c1;
	long c2;
	float weight;
};

static const struct sub_weight weights[] = {
	{'s', 'z', 0.5},
	{'u', 'y', 0.5},
	{'v', 'w', 0.5}
};

static const size_t weights_len = 3;

float min(float *array, size_t len);
float levenshtein_distance(wchar_t *s1, wchar_t *s2);
float weighted_sub(long c1, long c2);

float levenshtein_distance(wchar_t *s1, wchar_t *s2)
{
	size_t m = wcslen(s1);
	size_t n = wcslen(s2);
	if(m == 0)
		return n;
	if(n == 0)
		return m;
	m++, n++;
	float d[m][n];
	unsigned int i, j;
	for(i = 0; i < m; i++)
		for(j = 0; j < n; j++)
			d[i][j] = 0;
	for(i = 0; i < m; i++)
		d[i][0] = i;
	for(i = 0; i < n; i++)
		d[0][i] = i;
	for(i = 1; i < m; i++) {
		for(j = 1; j < n; j++) {
			//int sub = s1[i] == s2[j] ? 0 : 1;
			float sub = weighted_sub(s1[i - 1], s2[j - 1]);
			float dists[] = {d[i - 1][j] + 1, d[i][j - 1] + 1,
				d[i - 1][j - 1] + sub};
			d[i][j] = min(dists, 3);
		}
	}
	return d[m - 1][n - 1];
}

float weighted_sub(long c1, long c2)
{
	if(c1 == c2)
		return 0.0;
	unsigned int i;
	for(i = 0; i < weights_len; i++)
		if(c1 == weights[i].c1 || c1 == weights[i].c2)
			if(c2 == weights[i].c1 || c2 == weights[i].c2)
				return weights[i].weight;
	return 1.0;
}

float min(float *array, size_t len)
{
	unsigned int i;
	float cur_min = array[0];
	for(i = 1; i < len; i++)
		if(array[i] < cur_min)
			cur_min = array[i];
	return cur_min;
}

static PyObject *py_levenshtein_distance(PyObject *self, PyObject *args)
{
	const Py_UNICODE *s1;
	const Py_UNICODE *s2;
	if(!PyArg_ParseTuple(args, "uu", &s1, &s2))
		return NULL;
	float dist = levenshtein_distance((wchar_t*) s1, (wchar_t*) s2);
	return PyFloat_FromDouble(dist);
}

static struct PyMethodDef methods[] = {
	{"levenshtein_distance", py_levenshtein_distance,
	METH_VARARGS, "computes levenshtein distance"},
	{NULL, NULL, 0, NULL}
};

static struct PyModuleDef module = {
	PyModuleDef_HEAD_INIT,
	"editdistance",
	NULL,
	-1,
	methods
};

PyMODINIT_FUNC PyInit_editdistance(void)
{
	return PyModule_Create(&module);
}

int main(int argc, char *argv[])
{
	/*
	python 3.5:
	wchar_t *program = Py_DecodeLocale(argv[0], NULL);
	if(program == NULL) {
		fprintf(stderr, "error: program null\n");
		exit(1);
	}
	*/
	PyImport_AppendInittab("editdistance", PyInit_editdistance);
	Py_SetProgramName((wchar_t *)argv[0]);
	Py_Initialize();
	PyImport_ImportModule("editdistance");
	//PyMem_RawFree(program);
	return 0;
}
