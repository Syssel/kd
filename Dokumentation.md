
Københavns Diplomatarium
========================

Datastruktur
------------
---

__`RegisterData`__:

__Beskrivelse__: Færdigrediget register fra OCR for navne og sager i KD. Hver linje i filen har sit eget ID.

__Detaljer__:

```
CREATE TABLE RegisterData(
	id             	INTEGER  	PRIMARY KEY,
	content	    	VARCHAR  	NOT NULL,		
)
```

---


__`OpslagTabel`__:

__Beskrivelse__: Indeholder det parsede `RegisterData`.

__Detaljer__:

```
CREATE TABLE OpslagsTabel(
	id             	INTEGER  	PRIMARY KEY,
	opslagsform    	VARCHAR  	NOT NULL,	
	RegisterId     	INTEGER     NOT NULL,
	ParentId       	INTEGER  	DEFAULT NULL,	 
	variantformer  	VARCHAR     DEFAULT NULL,
	kommentar		VARCHAR     DEFAULT NULL,
	uncertain		INTEGER  	DEFAULT 0,
	
	FOREIGN KEY(RegisterId) REFERENCES RegisterData(id),
	FOREIGN KEY(ParentId)   REFERENCES OpslagsTabel(id),	
)
```

---

__`KildeformTabel`__:

__Beskrivelse__: Indeholder de fundne kildeform og deres kildehenvisning. Hver kildeform har henvisning til `OpslagsTabel`, som knytter kildeformen til et opslag og dermed registeret.

__Detaljer__:

```
CREATE TABLE KildeformTabel(
	id             	INTEGER  	PRIMARY KEY,
	OpslagId     	INTEGER     NOT NULL,
	bind		  	VARCHAR     NOT NULL,
	side			INTEGER     NOT NULL,
	uncertain		INTEGER  	DEFAULT 0,
	
	FOREIGN KEY(OpslagId)   REFERENCES OpslagsTabel(id),
)
```

---







<script src="http://yandex.st/highlightjs/7.3/highlight.min.js"></script>
<link rel="stylesheet" href="http://yandex.st/highlightjs/7.3/styles/github.min.css">
<script>
  hljs.initHighlightingOnLoad();
</script>