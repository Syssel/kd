import re

def has_references(line):
    """Checks whether any reference in <line>"""
    pattern = re.compile("([I|V|X]+[I|V|X]*)")
    if pattern.match(line):
        return True
    else:
        return False


def is_bind(bind):
    """Returns True other than bind-reference in <bind>"""
    pattern = re.compile('[I|V|X]*[I|V|X]*')
    if pattern.sub("", bind):
        return False
    else:
        return True


def contains(punct, line):
    """Returns True if any punct in <line>"""
    return any(p in line for p in punct)


def get_end_opslagsform(line):
    """Returns index of end of opslagsform in line.
       Corresponds to first found reference matching
       re-pattern. """
    pattern = re.compile("([I|V|X]+[I|V|X]*,)")
    try:
        return [m.start(0) for m in pattern.finditer(line)][0]

    except IndexError:
        return False


def clean_line(line):
    """Cleans string"""
    return line.strip().strip("—").strip()


def clean_string(string, accepted):
    """ Cleans string from every characters 
        except chars in accepted"""
    
    return re.sub(r'[^{}]'.format("".join(accepted)), '', string)


def str2num_range(string):
    """ Converts string of type '\d-\d' to python range.
        Inteprets string as a closed interval"""
    
    parts = re.sub(r'\s', '', string).split('-')
    assert len(parts) == 2, "String has to be of type '\d-\d'"
    m, n = len(parts[0]), len(parts[1])
    
    if m > n:
        return range(int(parts[0]), int(parts[0][0: (m-n)] + parts[1]) + 1)
    else: 
        return range(int(parts[0]), int(parts[1]) + 1)

        
# Punctuation data for opslag-parsing
punct = {'’', '(', ' eller ', '^', '«', ' og ', '>', ':', '»', '!', '<', '?', '-', ' el. ', "'", '=', ')', ';', ','}

spatial = {' i ', ' udenfor ', ' v. ', ' fra ', ' paa ', ' mod ', ' ved ', ' for '}
