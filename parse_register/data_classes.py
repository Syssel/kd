import numpy as np
from parse_methods import clean_line, get_end_opslagsform


    
class Kildeform(object):
    """Class object for storing kildeformer
       associated with Opslag"""

    def __init__(self, opslagId, bind, side, i_start, i_end):
        self.opslagId = opslagId
        self.bind     = bind
        self.side     = side

        self.i_start = i_start
        self.i_end   = i_end

        self.skal_redigeres = { "kommentar": True,
                                "error": True,
                                "side": True,
                              }
        
        self.kildeform   = None        
        self.uncertain = False


    def get_registerString(self, OpslagDict, RegisterDict):
        return OpslagDict[self.opslagId].get_kildehenvisninger(RegisterDict)[self.i_start: self.i_end]

        
class Opslag(object):
    """ Class object for storing Opslag together with its Kildeformer
        Accepts id-reference to KD-stednavneregister table"""
  

    def __init__(self, regId, parId = None):
        
        self.opslagsform     = ""
        self.opslagsform_end = -1
       
        self.registerId      = regId    
        self.parentId        = parId
        self.variantformer   = []        

        self.kommentar       = ""        
        
        self.skal_redigeres  = { "mangler_reference": True,
                                 "sammensat"    : True,
                                 "specialtegn"  : True,
                                 "lokalitet"    : True,
                               }
        
        self.parsed      = False    
        
        self.opslagstype = None
        

    def get_registerLine(self, data):
        return data[self.registerId]
    
    
    def get_kildehenvisninger(self, data):
        return self.get_registerLine(data)[self.opslagsform_end:]
    
    
    def add_kildeform(self, kildeform):
        self.kildeformer.append(kildeform)


    
fejl_beskrivelser = {
    "mangler_reference"   : "Ingen referencer i opslag",
    "sammensat"           : "Opslagsform skal sammensættes med andre registeropslag",
    "specialtegn"         : "Specialtegn i opslagsform der skal tolkes eller fejl i register",
    "lokalitet"           : "Lokalitetsbeskrivelse er indkorporeret i opslagsform",
    "kommentar"           : "Kommentar til kildehenvisning(er)",
    "error"               : "Kildeform kunne ikke tolkes",
    "side"                : "Side kunne ikke parses korrekt",
}



