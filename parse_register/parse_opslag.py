# -*- coding: utf-8 -*-
print("Loading modules...", flush=True)
import argparse
import os
import pickle
import numpy as np
import re
import time


from parse_methods import has_references, is_bind, get_end_opslagsform
from parse_methods import clean_line, punct, spatial, contains
from data_classes import Opslag, Kildeform




def parse_opslag(opslag, registerDict):
    
    # Error bools
    mangler_reference = True
    sammensat = True
    specialtegn = True
    lokalitet = True
    
    # Get raw data
    line = opslag.get_registerLine(registerDict)


    ### STEP 1: Isolate opslagsform
    end = get_end_opslagsform(line)
    if not end:
        end = len(line)
    
    opslagsform = line[:end].strip().strip(".")

    
    ### STEP 2: Check whether line has any references ###
    if has_references(line[end:]):
        mangler_reference = False
    
    
    ### STEP 3: Check if punctuation in opslagsform
    if not contains(punct, opslagsform):
        specialtegn = False
        
        
    ### STEP 4: Check if nested spatial description
    if not contains(spatial, opslagsform):
        lokalitet = False

        
    ### STEP 5: Mark if opslagsform has to be connected with previous lines from register
    if not opslag.parentId:
        sammensat = False
    
    
    opslag.skal_redigeres["mangler_reference"] = mangler_reference
    opslag.skal_redigeres["sammensat"] = sammensat
    opslag.skal_redigeres["specialtegn"] = specialtegn
    opslag.skal_redigeres["lokalitet"] = lokalitet

    
    opslag.opslagsform = opslagsform
    opslag.opslagsform_end = end   
    

    


def report(d):
    
    return d


if __name__ == "__main__":
    

    ### Read arguments from command line ###
    parser = argparse.ArgumentParser(description="""Parses KD-register into <data_classes.Opslag> 
                                                    instances. Outputs a) pickled dict of Opslag, b) 
                                                    pickled dict of RegisterData used by the parser, 
                                                    c) pickled dict of Kildeformer""")

    parser.add_argument("reg",  help="path to registerdata")
    parser.add_argument("name", help="name of output directory")
    args = parser.parse_args()
    
    print("Reading registerdata...", flush=True)    
    ### Load registerfile into opslagsgrupper (following the tab subordination) ###
    # Append cleaned data into array
    data = []

    with open(args.reg, "r") as f:
        for line in f.readlines():
            
            if line.startswith("\t\t—"):
                data[-1].append(clean_line(line))
            else:
                data.append([clean_line(line)])
    
    

    ### Create Opslag and registerData dicts ###
    opslagDict = dict()
    registerDict = dict()
    registerId = 0
    
    for gruppeId, register in enumerate(data):
        parentId = None
        
        for lineId, line in enumerate(register):     
            opslagDict[registerId] = Opslag(registerId, parentId)
            registerDict[registerId] = line
            
            if lineId == 0: parentId = registerId
            
            registerId += 1        

    print("Parse opslag...", flush=True)    
    ### Parse Opslag ###
    for instance in opslagDict.keys(): parse_opslag(opslagDict[instance], registerDict)
    
    
    print("Parse references...", flush=True)        
    ### Parse Kildeformer ###
    kildeformDict = {-1: None}

    for opslagsId, opslag in list(opslagDict.items()):

        kilder = opslag.get_kildehenvisninger(registerDict)
        if opslag.skal_redigeres["mangler_reference"] == False and not kilder:
            raise ValueError("Cannot find references even though no error message in post")    
        
        if kilder:
            index = -1
            element = ""
            element_start = 0
            element_end = 0
            bind = None
            comment = False

            comments_notation = ["jfr", "se", "sml", "=", "—"]


            while kilder:
                element += kilder[index]
                element = element.strip()
                index += 1


                ### CONCATENATE COMMENT-STRING ELEMENTS ###
                if element.lower().strip(".").strip(",") in comments_notation:
                    end_comment = kilder.find(".", index)        
                    element += kilder[index:]
                    index = len(kilder)-1
                    comment = True


                ### CONCATENATE PARENTHESIS ELEMENTS ###    
                if element.startswith("("):

                    end_parenthesis = kilder.find(")", index)
                    new_index = kilder.find(" ", end_parenthesis)

                    element += kilder[index:new_index]
                    index = new_index
                    comment = True


                ### END OF ELEMENT ###
                if kilder[index] == " " or index == len(kilder)-1:

                    ### ELEMENT == BIND ###
                    ## => bind = element
                    if is_bind(element.strip(".").strip(",")):
                        element_end = index            
                        bind = element.strip(".").strip(",")

                        # Reset and continue index
                        element_start = index
                        element = ""

                    ### ELEMENT =! BIND ###
                    ## => Initiate kildeform ##

                    else:
                        kildeform = Kildeform(opslagsId, None, None, None, None)                    
                        
                        ## If element==page
                        try:
                            element_end = index
                            side = element.strip(".").strip(",")
                            side = int(side)

                            # Create kildeform
                            kildeform.bind = bind
                            kildeform.side = side
                            kildeform.i_start = element_start
                            kildeform.i_end   = element_end
                            kildeform.skal_redigeres["kommentar"] = False
                            kildeform.skal_redigeres["error"] = False
                            kildeform.skal_redigeres["side"] = False
                            kildeformDict[max(kildeformDict.keys())+1] = kildeform

                            # Reset and continue index
                            element_start = index
                            element = ""

                        except ValueError:

                            ## If ALPHA in element
                            # => error handle
                            num_pattern = re.compile('[\d]+')
                            alpha_pattern = re.compile('[\w]+')
                            sub_string = num_pattern.sub('', element.strip(".").strip(",")).strip()

                            if (alpha_pattern.findall(sub_string)):

                                # if not a comment
                                # => gather elements affected by an error in one element
                                #    and create kildeform with error
                                if not comment:

                                    element_end = kilder.find(".", element_start)
                                    element += kilder[index:element_end]

                                    # Create kildeform                        
                                    kildeform.i_start = element_start
                                    kildeform.i_end   = element_end
                                    kildeform.skal_redigeres["kommentar"] = False
                                    kildeform.skal_redigeres["error"] = True
                                    kildeform.skal_redigeres["side"] = False
                                    kildeformDict[max(kildeformDict.keys())+1] = kildeform

                                    # Reset and move index
                                    index = element_end
                                    element_start = element_end+1
                                    element = ""                  

                                # else
                                # => create kildeform with error                    
                                else:

                                    element_end = index

                                    # Create kildeform                        
                                    kildeform.i_start = element_start
                                    kildeform.i_end   = element_end
                                    kildeform.skal_redigeres["kommentar"] = True
                                    kildeform.skal_redigeres["error"] = False
                                    kildeform.skal_redigeres["side"] = False
                                    kildeformDict[max(kildeformDict.keys())+1] = kildeform

                                    # Reset and continue index
                                    element_start = index                        
                                    element = ""                    
                                    comment = False

                            ## If ALPHA not in element 
                            # create kildeform with error
                            else:
                                element_end = index                  
                                side = str(element.strip(".").strip(","))

                                # Create kildeform
                                kildeform.bind = bind
                                kildeform.side = side
                                kildeform.i_start = element_start
                                kildeform.i_end   = element_end
                                kildeform.skal_redigeres["kommentar"] = False
                                kildeform.skal_redigeres["error"] = False
                                kildeform.skal_redigeres["side"] = True
                                kildeformDict[max(kildeformDict.keys())+1] = kildeform

                                # Reset and continue index
                                element_start = index
                                element = ""                    


                ### TERMINATE ###
                if index == len(kilder)-1 or index==-1:
                    kilder = ""


    # Create pickled data
    fpath = os.getcwd()+"/"+args.name+"/"
    print("Pickle dicts to destination {}...".format(fpath), flush=True)    
    os.makedirs(os.path.dirname(fpath), exist_ok=True)

    f = open(fpath+"notes.txt", "w")
    f.write("Parsed created @ {}".format(time.strftime("%c")))
    pickle.dump(registerDict, open(fpath+"register.p", "wb"))
    pickle.dump(opslagDict, open(fpath+"opslag.p", "wb"))
    pickle.dump(kildeformDict, open(fpath+"kildeformer.p", "wb"))    



    
    
    
    
    
    
    
    
    
    