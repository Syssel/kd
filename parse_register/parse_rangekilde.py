import argparse
import os
import pickle
import numpy as np
import time
import copy

from parse_methods import *
from data_classes import *



if __name__ == "__main__":


    ### Read arguments from command line ###
    parser = argparse.ArgumentParser(description="""Re-parses kildeformer.p from input directory""")
    parser.add_argument("name", help="name of input directory")
    args = parser.parse_args()

    ### Open files ###
    fpath = os.getcwd()+"/"+args.name+"/"
    kildeformDict = pickle.load(open(fpath+"kildeformer.p", "rb"))
    opslagDict    = pickle.load(open(fpath+"opslag.p", "rb"))
    registerDict  = pickle.load(open(fpath+"register.p", "rb"))



    ### Begin parse ###

    # List over accepted character not to be stripped, i.e. to be intepreted
    accepted = ["0-9", "(", ")", "?", "-", "a-z", "A-Z"]
    
    for i in range(len(kildeformDict)-1):
        
        if kildeformDict[i].skal_redigeres["side"] == True:
            
            string = kildeformDict[i].side
            string = clean_string(string, accepted)
            parts = string.split("-")
            
            if len(parts) == 2: 
                
                for side in str2num_range(string):
                    kildeform = kildeformDict[i]
                    kildeform.skal_redigeres["side"] = False
                    kildeform.side = side
                    kildeformDict[max(kildeformDict.keys())+1] = copy.deepcopy(kildeform)
                
                kildeformDict.pop(i)

    
    ### Pickle data ###
    pickle.dump(kildeformDict, open(fpath+"kildeformer.p", "wb"))

    ### Write logger ###
    logger = open(fpath+"notes.txt","r+")
    logger.read()
    logger.write("(parse_rangekilde) Parse edited @ {}".format(time.strftime("%c")))
    logger.close()


